﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using TrackerLogic.Peripherals;
using Unosquare.RaspberryIO;
using Unosquare.WiringPi;

namespace TrackerLogic
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Pi.Init<BootstrapWiringPi>();
            RFID.ReadCard();
        }
    }
}
