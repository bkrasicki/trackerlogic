﻿using Newtonsoft.Json;
using Swan;
using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using TrackerLogic.Peripherals;
using TrackerLogic.Requests;
using Unosquare.RaspberryIO;
using Unosquare.RaspberryIO.Peripherals;

namespace TrackerLogic.Peripherals
{
    public static class RFID
    {

        public static void ReadCard()
        {
            Console.Clear();
            var device = new RFIDControllerMfrc522(Pi.Spi.Channel0, 500000, Pi.Gpio[18]);
            Console.WriteLine("Card reader is running.");
            Console.WriteLine("Put card on the reader.");

            while (true)
            {

                // If a card is found
                if (device.DetectCard() != RFIDControllerMfrc522.Status.AllOk) continue;

                // Get the UID of the card
                var uidResponse = device.ReadCardUniqueId();

                // If we have the UID, continue
                if (uidResponse.Status != RFIDControllerMfrc522.Status.AllOk) continue;

                var cardUid = uidResponse.Data;

                // Print UID
                var cardnumber = $"{cardUid[0]}{cardUid[1]}{cardUid[2]}{cardUid[3]}";
                Console.WriteLine($"\nCheck worker with card id: {cardnumber}");

                // Select the scanned tag
                device.SelectCardUniqueId(cardUid);

                // Authenticate sector
                if (device.AuthenticateCard1A(RFIDControllerMfrc522.DefaultAuthKey, cardUid, 19) == RFIDControllerMfrc522.Status.AllOk)
                {
                    Led.LedStart(Led.PIN_YELLOW);
                    Thread.Sleep(1000);

                    var state = FunctionsConnector.GetWorkerState(cardnumber);
                    
                    switch (state)
                    {
                        case 0:
                            Console.WriteLine("Unassigned card. Access denied.");
                            Led.LedStart(Led.PIN_RED);
                            break;
                        case 1:
                            Console.WriteLine("Worker has no access.");
                            Led.LedStart(Led.PIN_RED);
                            break;
                        case 2:
                            Console.WriteLine("Entry OK.");
                            Led.LedStart(Led.PIN_GREEN);

                            var logSendStatus = FunctionsConnector.SendEntryLog(cardnumber);
                            
                            if (logSendStatus == 404)
                            {
                                Console.WriteLine($"Entry information has not been sent to database. \nUser has no access or card is unassigned.");
                            }
                            else if (logSendStatus == 200)
                            {
                                Console.WriteLine("Entry information has been sent successful to database.");
                            }
                            else if (logSendStatus == 500)
                            {
                                Console.WriteLine("Entry information has not been sent to database. Connection error.");
                            }
                            break;
                        default:
                            break;
                    }

                    Thread.Sleep(3000);
                    Led.AllLedStop();
                    Console.WriteLine("Put card on the reader.");
                }
                else
                {
                    Led.LedStart(Led.PIN_RED);
                    Console.WriteLine("Card authentication error");
                    Thread.Sleep(3000);
                    Led.AllLedStop();
                    Console.WriteLine("\nPut card on the reader again.");
                }

                device.ClearCardSelection();

            }
        }

    }
}
