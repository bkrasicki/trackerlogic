﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Text;
using System.Threading;

namespace TrackerLogic.Peripherals
{
    public static class Led
    {
        public const int PIN_RED = 21;
        public const int PIN_GREEN = 26;
        public const int PIN_YELLOW = 6;

        public static void LedStart(int pinNumber)
        {
            GpioController controller = new GpioController();
            controller.OpenPin(pinNumber, PinMode.Output);
            controller.Write(pinNumber, PinValue.High);
        }

        public static void LedStop(int pinNumber)
        {
            GpioController controller = new GpioController();
            controller.OpenPin(pinNumber, PinMode.Output);
            controller.Write(pinNumber, PinValue.Low);
        }

        public static void AllLedStop()
        {
            GpioController controller = new GpioController();
            controller.OpenPin(PIN_GREEN, PinMode.Output);
            controller.OpenPin(PIN_RED, PinMode.Output);
            controller.OpenPin(PIN_YELLOW, PinMode.Output);
            controller.Write(PIN_GREEN, PinValue.Low);
            controller.Write(PIN_RED, PinValue.Low);
            controller.Write(PIN_YELLOW, PinValue.Low);
        }

        public static void LedTest()
        {
            var pinRed = 21;
            var pinGreen = 26;
            var pinYellow = 6;
            var lightTimeInMilliseconds = 1000;
            var dimTimeInMilliseconds = 200;
            using (GpioController controller = new GpioController())
            {
                controller.OpenPin(pinRed, PinMode.Output);
                controller.OpenPin(pinGreen, PinMode.Output);
                controller.OpenPin(pinYellow, PinMode.Output);
                Console.WriteLine($"GPIO pin enabled for use: {pinRed}");
                Console.WriteLine($"GPIO pin enabled for use: {pinGreen}");
                Console.WriteLine($"GPIO pin enabled for use: {pinYellow}");
                Console.CancelKeyPress += (object sender, ConsoleCancelEventArgs eventArgs) =>
                {
                    controller.Dispose();
                };
                while (true)
                {
                    Console.WriteLine($"Light for {lightTimeInMilliseconds}ms");
                    controller.Write(pinRed, PinValue.High);
                    controller.Write(pinGreen, PinValue.High);
                    controller.Write(pinYellow, PinValue.High);
                    Thread.Sleep(lightTimeInMilliseconds);
                    Console.WriteLine($"Dim for {dimTimeInMilliseconds}ms");
                    controller.Write(pinRed, PinValue.Low);
                    controller.Write(pinGreen, PinValue.Low);
                    controller.Write(pinYellow, PinValue.Low);
                    Thread.Sleep(dimTimeInMilliseconds);
                }
            }
        }
    }
}
