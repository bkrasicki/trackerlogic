﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace TrackerLogic.Requests
{
    public static class FunctionsConnector
    {

        public static int GetWorkerState(string cardnumber)
        {
            var httpClient = new HttpClient();
            var response = httpClient.GetStringAsync($"http://worktrackerfunctionapp.azurewebsites.net/api/worker/{cardnumber}?code=g0yQyLmFUDAzVK6d7oit0ZdvyP55d3hKIe43/KzSdlmdPRWQV9KWwg==");
            dynamic state = JsonConvert.DeserializeObject(response.Result);
            return state["state"];
        }

        public static int SendEntryLog(string cardnumber)
        {
            var httpClient = new HttpClient();
            var workerContent = new { cardNumber = cardnumber };
            var contentJson = JsonConvert.SerializeObject(workerContent);
            var contentToSend = new StringContent(contentJson, Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync("http://worktrackerfunctionapp.azurewebsites.net/api/worker/addentryhistory?code=bzRY6xBc5EVLxzV5v9bzGwGCaPTPr1oihRMDX51s4dX4GqIn/P9Pcw==", contentToSend);
            return (int) response.Result.StatusCode;
        }
    }
}
